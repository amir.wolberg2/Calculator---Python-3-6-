"""
Module containing all characters supported by the calculator
"""
from operations import SupportedOperators
from parenthesis import SupportedOpeningParenthesis, SupportedClosingParenthesis

'''
Holds all characters supported by the calculator
'''
SupportedCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".",
                       " "] + SupportedClosingParenthesis \
                      + SupportedOpeningParenthesis + SupportedOperators
