"""
Module Containing all Supported opening and closing parenthesis types
"""

'''
Holds all supported parenthesis types
If parenthesis types are added add parenthesis of same type in the
same index (in each respective list)
'''
SupportedOpeningParenthesis = ["("]
SupportedClosingParenthesis = [")"]
