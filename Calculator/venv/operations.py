"""
Module containing all supported operators
"""
from exceptions import *
from abc import ABC, abstractmethod
from placements import Placement


'''
How to build and add Operator:

Placement:
Placement.LEFT -> operator_value
Placement.RIGHT -> value_operator
Placement.BETWEEN -> value_operator_value
Placement.SIGN-> combines left and between and can also
 be chained like right (can come between 2 numbers,
  before a number and after another operator)

Precedence:
must be a natural number for example-
1/2/3/4/5/6....

Calculate:
function that returns result

Add the Operator to the Operators dictionary with a chosen character as
key and the class as value like so:
"Character": CharacterOperatorClass()
'''


class Operator(ABC):
    """ Template for operator """

    @abstractmethod
    def __init__(self):
        """
        Initiates the operator and sets the placement and precedence
        """
        self.placement = None  # Placement enum value
        self.precedence = None  # int value representing precedence

    @abstractmethod
    def calculate(self, a, b):
        """
        Does the calculation according to the operator
        :param a: left value (located to the left of operator)
        :param b: right value (located to the right of operator)
        :return: get result of operation
        """
        return None


'''
All operator classes
| | | | |
v v v v v
'''
# Precedence 1


class Sub(Operator):
    """ Subtract operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.SIGN
        self.precedence = 1

    def calculate(self, a, b):
        """
           -
           subtracts b from a
           :param a: left value
           :param b: right value
           :return: a - b
           """
        return float(a) - float(b)


class Add(Operator):
    """ Add operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.BETWEEN
        self.precedence = 1

    def calculate(self, a, b):
        """
        +
        Adds a to b
        :param a: left value
        :param b: right value
        :return: a + b
        """
        return float(a) + float(b)

# Precedence 2


class Mul(Operator):
    """ Multiply operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.BETWEEN
        self.precedence = 2

    def calculate(self, a, b):
        """
        *
        Multiplies a with b
        :param a: left value
        :param b: right value
        :return: a * b
        """
        return float(a) * float(b)


class Div(Operator):
    """ Divide operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.BETWEEN
        self.precedence = 2

    def calculate(self, a, b):
        """
        /
        Divides a by b
        :param a: left value
        :param b: right value
        :return: a / b
        """
        return float(a) / float(b)

# Precedence 3


class Pow(Operator):
    """ Power operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.BETWEEN
        self.precedence = 3

    def calculate(self, a, b):
        """
        ^
        Puts a to the power of b
        :param a: left value
        :param b: right value
        :return: a ** b
        """
        return float(a) ** float(b)

# Precedence 4


class Mod(Operator):
    """ Module operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.BETWEEN
        self.precedence = 4

    def calculate(self, a, b):
        """
        %
        Gets the module a by b
        :param a: left value
        :param b: right value
        :return: a % b
        """
        return a % b

# Precedence 5


class Avg(Operator):
    """ Average operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.BETWEEN
        self.precedence = 5

    def calculate(self, a, b):
        """
        @
        Gets the average between a and b
        :param a: left value
        :param b: right value
        :return: (a + b) / 2.0
        """
        return (a + b) / 2.0


class Min(Operator):
    """ Minimum operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.BETWEEN
        self.precedence = 5

    def calculate(self, a, b):
        """
        &
        The smaller number out of a and b
        :param a: left value
        :param b: right value
        :return: a*(a <= b) + b*(a > b)
        """
        return float(a) * (a <= b) + float(b) * (a > b)


class Max(Operator):
    """ Maximum operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.BETWEEN
        self.precedence = 5

    def calculate(self, a, b):
        """
        $
        The bigger number out of a and b
        :param a: left value
        :param b: right value
        :return: a*(a > b) + b*(a <= b)
        """
        return float(a) * (a > b) + float(b) * (a <= b)

# Precedence 6


class Fac(Operator):
    """ Factorial operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.RIGHT
        self.precedence = 6

    def calculate(self, a, b=None):
        """
        !
        Returns the factorial of a
        :param b: None -> operator receives only 1 parameter
        :param a: left value
        :return: a!
        """
        try:
            a_int = int(a)
        except ValueError:
            raise FactorialException("Can only factorial natural numbers")

        # Check if number sent to fac is not a whole number
        if a_int != a:
            raise FactorialException("Can only factorial natural numbers"
                                     " (this number is not whole)")

        # Check if number sent to fac is a negative number
        if a_int < 0:
            raise FactorialException("Can only factorial natural numbers"
                                     " (this number is not positive)")

        factorial = 1
        for x in range(1, a_int + 1):
            factorial = factorial * x

        return float(factorial)


class Neg(Operator):
    """ Negative operator """

    def __init__(self):
        """
        Set placement and precedence
        """
        Operator.__init__(self)
        self.placement = Placement.LEFT
        self.precedence = 6

    def calculate(self, a, b=None):
        """
        ~
        Subtracts a from 0
        :param b: None -> operator receives only 1 parameter
        :param a: right value
        :return: -a
        """
        return 0 - a


'''
Dictionary containing all possible operators supported by the system
{Operator: Operator class instance}
'''
Operators = {"-": Sub(), "+": Add(), "*": Mul(), "/": Div(),
             "^": Pow(), "%": Mod(), "@": Avg(), "&": Min(),
             "$": Max(), "!": Fac(), "~": Neg()}

'''
Contains all operator's characters supported by the calculator
'''
SupportedOperators = list(Operators.keys())
