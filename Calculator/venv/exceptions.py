"""
Module containing all different costume made exception types
"""


class FactorialException(Exception):
    """ The class representing the factorial operator's exceptions """
    pass


class NotSupportedException(Exception):
    """ The class representing a not supported feature exception """
    pass
