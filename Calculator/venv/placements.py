"""
Module containing placements Enum Declaration
"""
from enum import Enum
'''
How to add placement option:

Add property to Placement Enum

Create _cal function for that placement in calculations

Add to PlacementCalculations dictionary Placement.property as key
and the _cal function as value like so:
Placement.property: _cal_property

'''


class Placement(Enum):
    """ Enum of possible operator placements """

    # Operator_Value
    LEFT = "left"
    # Value_Operator
    RIGHT = "right"
    # Value_Operator_Value
    BETWEEN = "between"
    # Can be chained or be alone before value Or come between 2 values
    SIGN = "sign"

