"""
Main module of the program ,
run this module to start the up the calculator
"""
from validation import check_validity, check_character_validity_str
from calculations import solve
from conversion import convert_to_list
from user_interface import *


def handle_equation(str_equation):
    """
    Calls all validation, conversion and calculation functions
    on a given string representing an equation
    :param str_equation: string form of the equation given by the user
    :return: the result of the equation or
    throw an appropriate exception
    """

    # Limit equation length to 100,000 characters
    if len(str_equation) > 100000:
        raise Exception("Equation exceeded maximum length allowed"
                        " (must be under 100,000 characters in the equation)")

    # Check Character validity of the given string
    check_character_validity_str(str_equation)

    # Convert equation to a list for the solving process
    lst_equation = convert_to_list(str_equation)

    # Check pre calculation validity of the equation
    check_validity(lst_equation)

    # Solve equation, if exception occurs throw it
    try:
        # Solve equation
        return solve(lst_equation)
    except RecursionError:
        raise Exception("Equation too long, Maximum recursion depth exceeded")


def _get_ui():
    """
    Gets input as to what user interface to use for the calculator
    :return: chosen user interface OR x meaning to exit program
    """
    # Chosen user interface
    ui = ""

    # Remains true until user chooses valid interface or exits program
    ui_not_chosen = True

    # Loop until a valid UI or X is chosen by the user
    while ui_not_chosen:
        try:
            ui = input(
                "Enter what user interface you want to use out of given options"
                + str(UI_supported_options) + " Or enter X to exit program:")
        except Exception as error:
            # Input crashed -> exit program
            print("input crashed: " + str(error))
            return "x"

        # Exiting program
        if ui.lower() == "x":
            return "x"

        # User interface not supported
        elif ui not in UI_supported_options:
            print("Unsupported user interface option chosen")

        # User interface chosen -> proceed
        else:
            ui_not_chosen = False

    # Return chosen user interface
    return ui


if __name__ == "__main__":

    # Gets user interface
    UI = _get_ui()

    # If valid interface was chosen
    if UI != 'x':
        try:
            # Initiating user interface
            UI_options[UI](handle_equation)
        except Exception as e:
            # User interface crashed informing user and exiting program
            print("User interface crashed: " + str(e))

    print("Exiting program...")
